# == Class: pwdsettings
#
# Configures password settings and expirations per Jabil Standard
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { pwdsettings:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Steve Illgen <steven_illgen@jabil.com
#
# === Copyright
#
# Copyright 2014 Jabil Circuits, unless otherwise noted.
#
class pwdsettings {
   file { '/etc/pam.d/password-auth-ac':
   ensure       => 'present',
   source       => 'puppet:///modules/pwdsettings/password-auth-ac',
   mode         => '0644',
   }

   file { '/etc/pam.d/system-auth-ac':
   ensure       => 'present',
   source       => 'puppet:///modules/pwdsettings/system-auth-ac',
   mode         => '0644',
   }

   file { '/etc/login.defs':
   ensure       => 'present',
   source       => 'puppet:///modules/pwdsettings/login.defs',
   mode         => '0644',
   }
}
